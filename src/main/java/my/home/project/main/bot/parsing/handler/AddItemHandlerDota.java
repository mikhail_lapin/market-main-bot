package my.home.project.main.bot.parsing.handler;

import my.home.project.main.bot.parsing.model.AddItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AddItemHandlerDota extends NotificationHandler<AddItem.Data> {

    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    @Inject
    public AddItemHandlerDota(@Value("my.home.project.main.bot.parsing.model.AddItem.Data") Class<AddItem.Data> typeParameterClass) {
        super(typeParameterClass);
    }

    @Override
    protected void handle(AddItem.Data notification) {
        if (notification.getUiStatus() == 1) {
            System.out.println(dateFormat.format(new Date()) + " " + notification.getName() + " выставлен за: " + notification.getUiPrice() + " руб.");
        } else if (notification.getUiStatus() == 3) {
            System.out.println(dateFormat.format(new Date()) + " " + notification.getName() + " куплен за: " + notification.getUiPrice() + " руб.");
        }
    }
    
}
