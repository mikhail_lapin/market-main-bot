package my.home.project.main.bot.parsing.handler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class MoneyHandler extends NotificationHandler<String> {

    @Inject
    public MoneyHandler(@Value("java.lang.String") Class<String> typeParameterClass) {
        super(typeParameterClass);
    }

    @Override
    protected void handle(String notification) {

        String result = notification.replaceAll("[^0-9.]", "");

        Double money = Double.valueOf(result);

        System.out.println("На счету: " + money + " руб.");
    }
}
