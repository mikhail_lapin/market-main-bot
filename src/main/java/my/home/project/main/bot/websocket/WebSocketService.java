package my.home.project.main.bot.websocket;

import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.springframework.stereotype.Component;
import java.net.URI;

@Component
public class WebSocketService {

    public void getConnect() {

        WebSocketClient client = new WebSocketClient(new SslContextFactory(true));
        Handler hdlr = new Handler();

        try {
            client.start();
            URI sinkTarget = new URI("wss://wsn.dota2.net/wsn/");
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            client.connect(hdlr, sinkTarget, request);

        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

}
