package my.home.project.main.bot.steam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemToReceive {

    @JsonProperty("appid")
    private Integer appId;
    
    @JsonProperty("contextid")
    private Integer contextId;
    
    @JsonProperty("assetid")
    private Long assetId;
    
    @JsonProperty("classid")
    private Long classId;
    
    @JsonProperty("instanceid")
    private Long instanceId;

    @JsonProperty("amount")
    private Integer amount;
    
}