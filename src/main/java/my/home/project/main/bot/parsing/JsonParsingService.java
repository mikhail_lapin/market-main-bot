package my.home.project.main.bot.parsing;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class JsonParsingService {

    private final ObjectMapper mapper = new ObjectMapper();

    public <T> T parse(InputStream is, Class<T> clazz) throws IOException {
        if (is == null) {
            return null;
        }
        try {
            return mapper.readValue(is, clazz);
        } catch (Throwable th) {
            System.out.println("Ошибка в: parse " + th);
        }
        return null;
    }

    public <T> List<T> parseList(InputStream is, Class<T> clazz) throws IOException {
        if (is == null) {
            return null;
        }
        try {
            JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
            return mapper.readValue(is, type);
        } catch (Throwable th) {
            System.out.println("Ошибка в: parseList " + th);
        }
        return null;
    }

    public JsonNode parseTree(InputStream is) {
        if (is == null) {
            return null;
        }
        JsonNode jsonNode = null;
        try {
            jsonNode = mapper.readTree(is);
        } catch (Throwable th) {
            System.out.println("Ошибка в: parseTree " + th);
        }
        return jsonNode;
    }
    
}