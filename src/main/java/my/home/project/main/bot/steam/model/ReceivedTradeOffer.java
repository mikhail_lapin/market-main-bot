package my.home.project.main.bot.steam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceivedTradeOffer {

    @JsonProperty("tradeofferid")
    private long tradeOfferId;

    @JsonProperty("accountid_other")
    private int accountIdOther;

    @JsonProperty("message")
    private String message;

    @JsonProperty("trade_offer_state")
    private int tradeOfferState;
    
    @JsonProperty("items_to_receive")
    private List<ItemToReceive> itemsToReceiveList;
    
    @JsonProperty("items_to_give")
    private List<ItemToReceive> itemsToGive;
    
}
