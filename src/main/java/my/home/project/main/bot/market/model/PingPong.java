package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PingPong {
    
    @JsonProperty("success")
    private String pingStatus;

    @JsonProperty("ping")
    private String ping;

    @JsonProperty("error")
    private String error;
}
