package my.home.project.main.bot.market;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import my.home.project.main.bot.market.model.BuyItemInfo;
import my.home.project.main.bot.market.model.ItemInfo;
import my.home.project.main.bot.market.model.ItemToBuy;
import my.home.project.main.bot.market.model.NotificationList;
import my.home.project.main.bot.market.model.Order;
import my.home.project.main.bot.market.model.PingPong;
import my.home.project.main.bot.market.model.ProcessOrder;
import my.home.project.main.bot.market.model.RefreshInventory;
import my.home.project.main.bot.market.model.SetPriceInfo;
import my.home.project.main.bot.market.model.SteamOffer;
import my.home.project.main.bot.market.model.TradeInfo;
import my.home.project.main.bot.market.model.WSAuth;
import my.home.project.main.bot.parsing.JsonParsingService;
import my.home.project.main.bot.requester.SendingRequestService;
import my.home.project.main.bot.steam.model.SteamInventoryItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MarketRequestService {

    JsonParsingService jsonParsingService = new JsonParsingService();
    SendingRequestService sendingRequestService = new SendingRequestService();

    @Value("${marketKey}")
    private String marketKey;

    @Value("${marketUrlCS}")
    private String marketUrlCS;

    @Value("${marketUrlDota}")
    private String marketUrlDota;

    @Value("${marketUrlPubg}")
    private String marketUrlPubg;

    //Запрос PingPong(чтобы всегде быть онлайн на сайте)
    public PingPong sendPingPong(String gameName) {
        PingPong pingPongInfo = new PingPong();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/PingPong/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            pingPongInfo = jsonParsingService.parse(is, PingPong.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }

        return pingPongInfo;
    }

    //Запрос ItemInfo(информация и предложения о продаже конкретной вещи)
    public ItemInfo getItemInfo(long classId, long instanceId, String gameName) {
        ItemInfo itemInfo = new ItemInfo();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/ItemInfo/" + classId + "_" + instanceId + "/ru/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            itemInfo = jsonParsingService.parse(is, ItemInfo.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return itemInfo;
    }

    //Запрос Buy(покупка вещи)
    public BuyItemInfo buyItem(ItemToBuy itemToBuy, String gameName) {
        BuyItemInfo buyItemInfo = new BuyItemInfo();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/Buy/" + itemToBuy.getClassId() + "_" + itemToBuy.getInstanceId() + "/"
                + itemToBuy.getPrice() + "/" + itemToBuy.getHash() + "/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            buyItemInfo = jsonParsingService.parse(is, BuyItemInfo.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return buyItemInfo;
    }

    //Запрос ItemRequest(отправка оффлайн трейда от нашего бота вам)
    public SteamOffer sendSteamOffer(String inOut, long botId, String gameName) {
        SteamOffer steamOfferInfo = new SteamOffer();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/ItemRequest/" + inOut + "/" + botId + "/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            steamOfferInfo = jsonParsingService.parse(is, SteamOffer.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return steamOfferInfo;
    }

    //Запрос Trades(список предметов со страницы sell)
    public List<TradeInfo> getTrades(String gameName) {
        List<TradeInfo> tradeInfoList = new ArrayList();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/Trades/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            tradeInfoList = jsonParsingService.parseList(is, TradeInfo.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return tradeInfoList;
    }

    //Запрос SetPrice (Выставить новый предмет на продажу)
    public SetPriceInfo setPrice(SteamInventoryItem steamInventoryItem, int price, String gameName) {
        SetPriceInfo setPriceInfo = new SetPriceInfo();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/SetPrice/new_" + steamInventoryItem.getClassId() + "_" + steamInventoryItem.getInstanceId() + "/" + price + "?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
//        System.out.println(marketUrlCS + request);
        try {
            setPriceInfo = jsonParsingService.parse(is, SetPriceInfo.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return setPriceInfo;
    }

    //Запрос UpdateInventory(обновление инвентаря)
    public RefreshInventory refreshInventory(String gameName) {
        RefreshInventory refreshInventoryInfo = new RefreshInventory();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/UpdateInventory/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            refreshInventoryInfo = jsonParsingService.parse(is, RefreshInventory.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return refreshInventoryInfo;
    }

    //Запрос GetNotifications(список отслеживаемых вещей)
    public NotificationList getNotificationsInfo(String gameName) {
        NotificationList notificationList = new NotificationList();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/GetNotifications/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            notificationList = jsonParsingService.parse(is, NotificationList.class);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return notificationList;
    }

    //Запрос GetWSAuth (получение ключа для подписки на вебсокеты)
    public WSAuth getWSAuth() {
        WSAuth auth = new WSAuth();
        String request = "/api/GetWSAuth/?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrlCS + request);
        try {
            auth = jsonParsingService.parse(is, WSAuth.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return auth;
    }

    public List<Order> getOrders(String gameName) {
        List<Order> orders = new ArrayList<>();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/GetOrders//?key=" + marketKey;
        InputStream is = sendingRequestService.sendRequest(marketUrl + request);
        try {
            orders = jsonParsingService.parseList(is, Order.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return orders;
    }

    public ProcessOrder processOrder(long classId, long instanceId, int price, String gameName) {
        ProcessOrder processOrder = new ProcessOrder();
        String marketUrl = "";
        switch (gameName) {
            case "CsGo":
                marketUrl = marketUrlCS;
                break;
            case "Dota":
                marketUrl = marketUrlDota;
                break;
            case "Pubg":
                marketUrl = marketUrlPubg;
                break;
            default:
                return null;
        }
        String request = "/api/InsertOrder/" + classId + "/" + instanceId + "/" + price + "/?key=" + marketKey;
        InputStream is = sendingRequestService.sendTestRequest(marketUrl, request);
        try {
            processOrder = jsonParsingService.parse(is, ProcessOrder.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processOrder;
    }

}
