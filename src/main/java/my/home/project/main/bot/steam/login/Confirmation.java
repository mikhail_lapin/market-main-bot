package my.home.project.main.bot.steam.login;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import my.home.project.main.bot.steam.login.model.ConfirmResult;
import my.home.project.main.bot.steam.login.model.ConfirmationInfo;

@Component
public class Confirmation {

    @Autowired
    CookieService cookieService;

    @Autowired
    Totp totp;

    @Value("${deviceId}")
    private String deviceId;

    @Value("${steamId}")
    private String steamId;

    public List<ConfirmationInfo> getConfirmations(Long time) throws IOException {

        String confirmationKey = totp.getConfirmationKey(time, "conf");
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://steamcommunity.com/mobileconf/conf?p=android:" + deviceId + "&a=" + steamId + "&k=" + confirmationKey + "&t=" + time / 1000 + "&m=android&tag=conf");
        httpGet.setHeader("X-Requested-With", "com.valvesoftware.android.steam.community");
        httpGet.setHeader("Referer", "https://steamcommunity.com/mobilelogin?oauth_client_id=DE45CD61&oauth_scope=read_profile%20write_profile%20read_client%20write_client");
        httpGet.setHeader("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        httpGet.setHeader("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
        HttpContext localContext = new BasicHttpContext();
        //Вставляем Cookie в HttpContext
        localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieService.getCookieStore());
        CloseableHttpResponse response = httpclient.execute(httpGet, localContext);
        HttpEntity entity = response.getEntity();
        Document doc = Jsoup.parse(EntityUtils.toString(entity));
        List<ConfirmationInfo> confirmationInfos = new ArrayList<>();
        for (Element mobileConfList : doc.select("#mobileconf_list")) {
            for (Element mobileConf : mobileConfList.select(".mobileconf_list_entry")) {
                Elements description = mobileConf.select(".mobileconf_list_entry_description");
                Elements divTag = description.select("div");
                confirmationInfos.add(
                        new ConfirmationInfo(
                                mobileConf.attr("id"),
                                mobileConf.attr("data-confid"),
                                mobileConf.attr("data-key"),
                                divTag.get(1).text(),
                                divTag.get(2).text(),
                                divTag.get(3).text()
                        )
                );
            }
        }
        return confirmationInfos;
    }

    public ConfirmResult acceptConfirm(Long time, String confID, String confKey) {

        String confKeyTotp = totp.getConfirmationKey(time, "allow");
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://steamcommunity.com/mobileconf/ajaxop?p=android:" + deviceId + "&a=" + steamId + "&k=" + confKeyTotp + "&t=" + time / 1000 + "&op=allow&cid=" + confID + "&ck=" + confKey + "&m=android&tag=allow");
        httpGet.setHeader("X-Requested-With", "com.valvesoftware.android.steam.community");
        httpGet.setHeader("Referer", "https://steamcommunity.com/mobilelogin?oauth_client_id=DE45CD61&oauth_scope=read_profile%20write_profile%20read_client%20write_client");
        httpGet.setHeader("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        httpGet.setHeader("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
        HttpContext localContext = new BasicHttpContext();
//      Вставляем Cookie в HttpContext
        localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieService.getCookieStore());
        CloseableHttpResponse response = null;
        try {
            response = httpclient.execute(httpGet, localContext);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        }
        ConfirmResult confirmResult = new ConfirmResult();
        if (entity != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                confirmResult = objectMapper.readValue(entity.getContent(), new TypeReference<ConfirmResult>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return confirmResult;
    }

}
