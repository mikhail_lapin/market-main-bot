package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeInfo {
    
    @JsonProperty("ui_id")
    private long id;

    @JsonProperty("i_name")
    private String name;

    @JsonProperty("i_rarity")
    private String rarity;

    @JsonProperty("ui_status")
    private int status;

    @JsonProperty("he_name")
    private String heroName;

    @JsonProperty("ui_price")
    private double price;

    @JsonProperty("i_classid")
    private long classid;

    @JsonProperty("i_instanceid")
    private long instanceid;

    @JsonProperty("i_market_hash_name")
    private String marketHashName;

    @JsonProperty("i_market_name")
    private String marketName;

    @JsonProperty("i_market_price")
    private double recommendedPrice;

    @JsonProperty("min_price")
    private double minPrice;

    @JsonProperty("position")
    private int position;

    @JsonProperty("ui_bid")
    private long botId;

//    @JsonProperty("i_descriptions")
//    private List<Description> descriptions;

    @JsonProperty("error")
    private String error;
    
    @JsonProperty("left")
    private int left;

}
