package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {

    @JsonProperty("i_classid")
    protected long classId;

    @JsonProperty("i_instanceid")
    protected long instanceId;

    @JsonProperty("i_market_hash_name")
    protected String marketHashName;

    @JsonProperty("o_price")
    protected int price;

    @JsonProperty("o_state")
    protected int state;

}
