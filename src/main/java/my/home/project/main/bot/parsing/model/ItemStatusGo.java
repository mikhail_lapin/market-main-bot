package my.home.project.main.bot.parsing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

public class ItemStatusGo extends Notification<ItemStatusGo.Data> {

    @lombok.Data
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data implements NotificationData {

        @JsonProperty("id")
        private int id;

        @JsonProperty("status")
        private int status;

        @JsonProperty("bid")
        private int bid;

        @JsonProperty("left")
        private int left;

    }
}
