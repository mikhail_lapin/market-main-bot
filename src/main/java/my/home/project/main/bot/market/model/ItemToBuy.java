package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemToBuy {

    @JsonProperty("classid")
    protected long classId;

    @JsonProperty("instanceid")
    protected long instanceId;

    @JsonProperty("market_name")
    protected String marketName;

    @JsonProperty("price")
    protected int price;

    @JsonProperty("count")
    protected int count;

    @JsonProperty("hash")
    protected String hash;


}
