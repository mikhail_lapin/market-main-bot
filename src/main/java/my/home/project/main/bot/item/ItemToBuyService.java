package my.home.project.main.bot.item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import my.home.project.main.bot.market.MarketRequestService;
import my.home.project.main.bot.market.model.ItemInfo;
import my.home.project.main.bot.market.model.ItemToBuy;
import my.home.project.main.bot.market.model.NotificationInfo;
import my.home.project.main.bot.market.model.NotificationList;
import my.home.project.main.bot.parsing.JsonParsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemToBuyService {

    @Autowired
    MarketRequestService marketRequestsService;

    private Map<String, ItemToBuy> itemToBuyMapCsGo = new HashMap<>();
    private Map<String, ItemToBuy> itemToBuyMapIdsCsGo = new HashMap<>();
    private List<ItemToBuy> itemToBuyListNotificationCsGo = new ArrayList<>();
    private Map<String, ItemToBuy> itemToBuyMapDota = new HashMap<>();
    private Map<String, ItemToBuy> itemToBuyMapIdsDota = new HashMap<>();
    private List<ItemToBuy> itemToBuyListNotificationDota = new ArrayList<>();
    private Map<String, ItemToBuy> itemToBuyMapPubg = new HashMap<>();
    private Map<String, ItemToBuy> itemToBuyMapIdsPubg = new HashMap<>();
    private List<ItemToBuy> itemToBuyListNotificationPubg = new ArrayList<>();

    JsonParsingService jsonParsingService = new JsonParsingService();

    @PostConstruct
    public void readItemToBuyList() {
        try {
            NotificationList notificationList = null;
            NotificationList notificationListDota = null;
            NotificationList notificationListPubg = null;
            List<NotificationInfo> notificationsInfoList = new ArrayList<>();
            notificationList = marketRequestsService.getNotificationsInfo("CsGo");
            notificationListDota = marketRequestsService.getNotificationsInfo("Dota");
            notificationListPubg = marketRequestsService.getNotificationsInfo("Pubg");
            if (notificationList != null) {
                for (NotificationInfo notificationsInfo : notificationList.getNotificationsInfoList()) {
                    ItemInfo itemInfo = marketRequestsService.getItemInfo(notificationsInfo.getClassid(), notificationsInfo.getInstanceid(), "CsGo");
                    if (itemInfo != null) {
                        itemToBuyListNotificationCsGo.add(new ItemToBuy(itemInfo.getClassid(), itemInfo.getInstanceid(), notificationsInfo.getIMarketHashName(), notificationsInfo.getPrice(), 1, ""));
                    } else {
                        System.out.println(notificationsInfo.getIMarketHashName() + " - не записался.");
                    }
                }
            } else {
                System.out.println("notificationsInfoList CsGo равен null");
            }
            if (notificationListDota != null) {
                for (NotificationInfo notificationsInfo : notificationListDota.getNotificationsInfoList()) {
                    ItemInfo itemInfo = marketRequestsService.getItemInfo(notificationsInfo.getClassid(), notificationsInfo.getInstanceid(), "Dota");
                    if (itemInfo != null) {
                        itemToBuyListNotificationDota.add(new ItemToBuy(itemInfo.getClassid(), itemInfo.getInstanceid(), notificationsInfo.getIMarketHashName(), notificationsInfo.getPrice(), 1, itemInfo.getHash()));
                    } else {
                        System.out.println(notificationsInfo.getIMarketHashName() + " - не записался.");
                    }
                }
            } else {
                System.out.println("notificationsInfoList Dota равен null");
            }
            if (notificationListPubg != null) {
                for (NotificationInfo notificationsInfo : notificationListPubg.getNotificationsInfoList()) {
                    ItemInfo itemInfo = marketRequestsService.getItemInfo(notificationsInfo.getClassid(), notificationsInfo.getInstanceid(), "Pubg");
                    if (itemInfo != null) {
                        itemToBuyListNotificationPubg.add(new ItemToBuy(itemInfo.getClassid(), itemInfo.getInstanceid(), notificationsInfo.getIMarketHashName(), notificationsInfo.getPrice(), 1, itemInfo.getHash()));
                    } else {
                        System.out.println(notificationsInfo.getIMarketHashName() + " - не записался.");
                    }
                }
            } else {
                System.out.println("notificationsInfoList Pubg равен null");
            }

            if (itemToBuyListNotificationCsGo != null) {
                for (ItemToBuy itemToBuy : itemToBuyListNotificationCsGo) {
                    itemToBuyMapCsGo.put(itemToBuy.getMarketName(), itemToBuy);
                    itemToBuyMapIdsCsGo.put(itemToBuy.getClassId() + "_" + itemToBuy.getInstanceId(), itemToBuy);
                }
                System.out.println("Список закупок перезаписан!");
            } else {
                System.out.println("itemToBuy пустой");
            }
            if (itemToBuyListNotificationDota != null) {
                for (ItemToBuy itemToBuy : itemToBuyListNotificationDota) {
                    itemToBuyMapDota.put(itemToBuy.getMarketName(), itemToBuy);
                    itemToBuyMapIdsDota.put(itemToBuy.getClassId() + "_" + itemToBuy.getInstanceId(), itemToBuy);
                }
                System.out.println("Список закупок перезаписан!");
            } else {
                System.out.println("itemToBuy пустой");
            }
            if (itemToBuyListNotificationPubg != null) {
                for (ItemToBuy itemToBuy : itemToBuyListNotificationPubg) {
                    itemToBuyMapPubg.put(itemToBuy.getMarketName(), itemToBuy);
                    itemToBuyMapIdsPubg.put(itemToBuy.getClassId() + "_" + itemToBuy.getInstanceId(), itemToBuy);
                }
                System.out.println("Список закупок перезаписан!");
            } else {
                System.out.println("itemToBuy пустой");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ItemToBuy> getItemToBuyListNotificationCsGo() {
        return itemToBuyListNotificationCsGo;
    }

    public void setItemToBuyListNotificationCsGo(List<ItemToBuy> itemToBuyListNotificationCsGo) {
        this.itemToBuyListNotificationCsGo = itemToBuyListNotificationCsGo;
    }

    public Map<String, ItemToBuy> getItemToBuyMapCsGo() {
        return itemToBuyMapCsGo;
    }

    public Map<String, ItemToBuy> getItemToBuyMapIdsCsGo() {
        return itemToBuyMapIdsCsGo;

    }

    public List<ItemToBuy> getItemToBuyListNotificationDota() {
        return itemToBuyListNotificationDota;
    }

    public void setItemToBuyListNotificationDota(List<ItemToBuy> itemToBuyListNotificationDota) {
        this.itemToBuyListNotificationDota = itemToBuyListNotificationDota;
    }

    public Map<String, ItemToBuy> getItemToBuyMapDota() {
        return itemToBuyMapDota;
    }

    public Map<String, ItemToBuy> getItemToBuyMapIdsDota() {
        return itemToBuyMapIdsDota;

    }

    public void setItemToBuyListNotificationPubg(List<ItemToBuy> itemToBuyListNotificationPubg) {
        this.itemToBuyListNotificationPubg = itemToBuyListNotificationPubg;
    }

    public Map<String, ItemToBuy> getItemToBuyMapPubg() {
        return itemToBuyMapPubg;
    }

    public Map<String, ItemToBuy> getItemToBuyMapIdsPubg() {
        return itemToBuyMapIdsPubg;
    }

    public List<ItemToBuy> getItemToBuyListNotificationPubg() {
        return itemToBuyListNotificationPubg;
    }

}
