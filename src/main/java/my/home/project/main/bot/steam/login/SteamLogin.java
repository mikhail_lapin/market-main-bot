package my.home.project.main.bot.steam.login;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import my.home.project.main.bot.steam.login.model.DoLoginModel;
import my.home.project.main.bot.steam.login.model.RSAModel;

@Component
public class SteamLogin {

    @Autowired
    Totp totp;

    @Value("${steamLogin}")
    private String steamLogin;

    @Value("${steamPassword}")
    private String steamPassword;

    @Value("${steamMachineAuthName}")
    private String steamMachineAuthName;

    @Value("${steamMachineAuthValue}")
    private String steamMachineAuthValue;

    public RSAModel getRSAKey() {
        RSAModel rsaModel = new RSAModel();
        String url = "https://steamcommunity.com/login/getrsakey/";
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        String date = String.valueOf(new Date().getTime());
        nvps.add(new BasicNameValuePair("donotcache", date));
        nvps.add(new BasicNameValuePair("username", steamLogin));
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
        try {
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            ObjectMapper objectMapper = new ObjectMapper();
            rsaModel = objectMapper.readValue(entity.getContent(), new TypeReference<RSAModel>() {
            });
            System.out.println("Получение RSA Key: " + rsaModel.getSuccess());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rsaModel;
    }

    public CookieStore doLogin() throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        DoLoginModel doLoginModel = new DoLoginModel();
        String url = "https://steamcommunity.com/login/dologin/";
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        String date = String.valueOf(new Date().getTime());
        RSAModel rsaModel = getRSAKey();
        RSA rsa = new RSA(rsaModel.getPublicKeyMod(), rsaModel.getPublicKeyExp());
        String rsaTimeStamp = String.valueOf(rsaModel.getTimeStamp());
        String password = rsa.encrypt(steamPassword);
        String twofactorcode = totp.getAuthCode(System.currentTimeMillis());
        nvps.add(new BasicNameValuePair("donotcache", date));
        nvps.add(new BasicNameValuePair("password", password));
        nvps.add(new BasicNameValuePair("username", steamLogin));
        nvps.add(new BasicNameValuePair("twofactorcode", twofactorcode));
        nvps.add(new BasicNameValuePair("emailauth", ""));
        nvps.add(new BasicNameValuePair("loginfriendlyname", ""));
        nvps.add(new BasicNameValuePair("captchagid", "-1"));
        nvps.add(new BasicNameValuePair("captcha_text", ""));
        nvps.add(new BasicNameValuePair("emailsteamid", ""));
        nvps.add(new BasicNameValuePair("rsatimestamp", rsaTimeStamp));
        nvps.add(new BasicNameValuePair("remember_login", "true"));
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpClientContext context = HttpClientContext.create();
        HttpPost httpPost = new HttpPost(url);
        BasicClientCookie steamMachineAuth = new BasicClientCookie(steamMachineAuthName, steamMachineAuthValue);
        httpPost.setHeader("Cookie", steamMachineAuth.getName() + "=" + steamMachineAuth.getValue() + ";");
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
        try {
            CloseableHttpResponse response = httpclient.execute(httpPost, context);
            CookieStore cookieStore = context.getCookieStore();
            cookieStore.addCookie(steamMachineAuth);
            for (Cookie cookie : SteamLogin.getCookie(cookieStore).getCookies()) {
                cookieStore.addCookie(cookie);
            }
            HttpEntity entity = response.getEntity();
            ObjectMapper objectMapper = new ObjectMapper();
            doLoginModel = objectMapper.readValue(entity.getContent(), new TypeReference<DoLoginModel>() {
            });
            System.out.println("Получение Cookie в DoLogin: " + doLoginModel.getSuccess());
            if (doLoginModel.getSuccess()) {
                return cookieStore;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static CookieStore getCookie(CookieStore cookieStore) {
        String url = "http://steamcommunity.com/market/";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpClientContext context = HttpClientContext.create();
        HttpGet httpGet = new HttpGet(url);
        context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
        try {
            CloseableHttpResponse response = httpclient.execute(httpGet, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        cookieStore = context.getCookieStore();
        return cookieStore;
    }

}
