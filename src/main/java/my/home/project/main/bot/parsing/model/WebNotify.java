package my.home.project.main.bot.parsing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import my.home.project.main.bot.parsing.util.MapperUtils;

@ToString(callSuper = true)
public class WebNotify extends Notification<WebNotify.Data> {

    @lombok.Data
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data implements NotificationData {

        @JsonCreator
        public static Data create(String value) {

            return MapperUtils.parse(value, Data.class);

        }

        @JsonProperty("text")
        private String text;

        @JsonProperty("tag")
        private String tag;

        @JsonProperty("url")
        private String url;

    }

}
