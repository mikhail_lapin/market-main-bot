package my.home.project.main.bot.market.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MarketItemInfo {

    private long classid;

    private long instanceid;

    private String name;

    private int price;
    
    private Integer buyPrice;

}
