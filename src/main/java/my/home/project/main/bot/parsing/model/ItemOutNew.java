package my.home.project.main.bot.parsing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@ToString(callSuper = true)
public class ItemOutNew extends Notification<ItemOutNew.Data> {

    @lombok.Data
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data implements NotificationData {

        @JsonProperty("ui_id")
        private long uiId;
        @JsonProperty("i_name")
        private String iName;
        @JsonProperty("ui_status")
        private int uiStatus;
        @JsonProperty("he_name")
        private String heName;
        @JsonProperty("ui_price")
        private double uiPrice;
        @JsonProperty("i_classid")
        private long iClassId;
        @JsonProperty("i_instanceid")
        private long iInstanceid;
        @JsonProperty("ui_bid")
        private int uiBid;
        @JsonProperty("ui_uid")
        private long uiUid;
        @JsonProperty("clear")
        private boolean clear;

    }
}
