package my.home.project.main.bot.util;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Util {

    @Autowired
    private ApplicationContext appContext;

    public static ApplicationContext staticAppContext;

    @PostConstruct
    public void postConstruct() {
        staticAppContext = appContext;
    }

}
