package my.home.project.main.bot.steam.login.model;

public class ConfirmationInfo {

    private String id;
    private String confId;
    private String key;
    private String title;
    private String receiving;
    private String time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConfId() {
        return confId;
    }

    public void setConfId(String confId) {
        this.confId = confId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReceiving() {
        return receiving;
    }

    public void setReceiving(String receiving) {
        this.receiving = receiving;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ConfirmationInfo() {
    }

    public ConfirmationInfo(String id, String confId, String key, String title, String receiving, String time) {
        this.id = id;
        this.confId = confId;
        this.key = key;
        this.title = title;
        this.receiving = receiving;
        this.time = time;
    }

    @Override
    public String toString() {
        return "ConfirmationInfo{" +
                "id='" + id + '\'' +
                ", confId='" + confId + '\'' +
                ", key='" + key + '\'' +
                ", title='" + title + '\'' +
                ", receiving='" + receiving + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
