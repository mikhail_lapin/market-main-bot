package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetPriceInfo {

    @JsonProperty("result")
    private String result;

    @JsonProperty("item_id")
    private Long itemId;

    @JsonProperty("price")
    private Long price;

    @JsonProperty("price_text")
    private String priceText;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("position")
    private Integer position;

    @JsonProperty("success")
    private String success;

    @JsonProperty("error")
    private String error;
    
}
