package my.home.project.main.bot.parsing.handler;

import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import my.home.project.main.bot.item.ItemToBuyService;
import my.home.project.main.bot.market.MarketRequestService;
import my.home.project.main.bot.market.model.BuyItemInfo;
import my.home.project.main.bot.market.model.ItemInfo;
import my.home.project.main.bot.market.model.ItemToBuy;
import my.home.project.main.bot.parsing.model.NewItem;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class NewItemHandlerPubg extends NotificationHandler<NewItem.Data> {

    @Inject
    public NewItemHandlerPubg(@Value("my.home.project.main.bot.parsing.model.NewItem.Data") Class<NewItem.Data> typeParameterClass) {
        super(typeParameterClass);
    }

    @Autowired
    ItemToBuyService itemToBuyService;

    @Autowired
    MarketRequestService marketRequestsService;

    @Override
    protected void handle(NewItem.Data notification) {
        Map<String, ItemToBuy> itemToBuyMap = itemToBuyService.getItemToBuyMapPubg();
        try {
            if (itemToBuyMap.get(notification.getIMarketHashName()) != null) {
                ItemToBuy itemToBuy = itemToBuyMap.get(notification.getIMarketHashName());
                if (notification.getUiPrice() * 100 <= itemToBuy.getPrice()) {
                    ItemInfo itemInfo = marketRequestsService.getItemInfo(notification.getClassId(), notification.getInstanceId(), "Pubg");
                    if (itemInfo != null) {
                        BuyItemInfo buyItemInfo = marketRequestsService.buyItem(new ItemToBuy(itemInfo.getClassid(), itemInfo.getInstanceid(), notification.getIMarketName(), itemToBuy.getPrice(), 1, itemInfo.getHash()), "Pubg");
                        System.out.println("Попытка покупки " + itemInfo.getMarketName() + " через NewItemHandler" + buyItemInfo);
                    }
                }
            }
        } catch (Throwable th) {
            System.out.println("Проеба в: NewItemHandler");
        }
    }

}
