package my.home.project.main.bot.parsing.handler;

import my.home.project.main.bot.parsing.model.ItemOutNew;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;
import my.home.project.main.bot.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class ItemOutNewHandlerDota extends NotificationHandler<ItemOutNew.Data> {

    @Autowired
    TaskService taskService;
    
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    @Inject
    public ItemOutNewHandlerDota(@Value("my.home.project.main.bot.parsing.model.ItemOutNew.Data") Class<ItemOutNew.Data> typeParameterClass) {
        super(typeParameterClass);
    }

    @Override
    protected void handle(ItemOutNew.Data notification) {
        if (!notification.isClear()) {
            taskService.sellItemQueueDota.getAndIncrement();
            //Тут сумируем кол-во проданых пердметов
            System.out.println(dateFormat.format(new Date()) + " " + notification.getIName() + " продан за " + notification.getUiPrice() + " руб.");
        }

    }
}
