package my.home.project.main.bot.steam.login.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DoLoginModel {

    @JsonProperty("success")
    private Boolean success;

    @JsonProperty("requires_twofactor")
    private Boolean requiresTwoFactor;

    @JsonProperty("login_complete")
    private Boolean loginComplete;

    @JsonProperty("transfer_url")
    private String transferUrl;

    @JsonProperty("token")
    private String token;

    @JsonProperty("auth")
    private String auth;

    @JsonProperty("remember_login")
    private Boolean rememberLogin;

    @JsonProperty("token_secure")
    private String tokenSecure;

}
