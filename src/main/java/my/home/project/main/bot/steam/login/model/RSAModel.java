package my.home.project.main.bot.steam.login.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class RSAModel {

    @JsonProperty("success")
    private Boolean success;

    @JsonProperty("publickey_mod")
    private String publicKeyMod;

    @JsonProperty("publickey_exp")
    private String publicKeyExp;

    @JsonProperty("timestamp")
    private Long timeStamp;

    @JsonProperty("steamid")
    private Long steamId;

    @JsonProperty("token_gid")
    private String tokenGid;

}
