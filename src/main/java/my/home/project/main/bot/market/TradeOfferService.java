package my.home.project.main.bot.market;

import java.util.HashSet;
import java.util.List;

import my.home.project.main.bot.market.model.SteamOffer;
import my.home.project.main.bot.market.model.TradeInfo;
import my.home.project.main.bot.steam.SteamRequestsService;
import my.home.project.main.bot.steam.model.ReceivedTradeOffer;
import my.home.project.main.bot.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TradeOfferService {

    @Autowired
    MarketRequestService marketRequestsService;

    @Autowired
    SteamRequestsService steamRequestsService;

    @Autowired
    TaskService taskService;

    public void passSellItem(String gameName) {
        try {
            //Отправляем запрос на создание трейда маркету
            SteamOffer steamOffer = marketRequestsService.sendSteamOffer("in", 1, gameName);
            if (steamOffer != null) {
                if (steamOffer.isRequestStatus()) {
                    System.out.println("Оффлайн трейд отправлен, проверяй");
                    //Получаем список трейдов в steam
                    List<ReceivedTradeOffer> receivedTradeOfferList = steamRequestsService.getReceivedTradeOffers();
                    if (receivedTradeOfferList != null) {
                        for (ReceivedTradeOffer receivedTradeOffer : receivedTradeOfferList) {
                            //Проверяем соответствие трейдов
                            if (receivedTradeOffer.getTradeOfferId() == steamOffer.getIdSteamTrade()
                                    && receivedTradeOffer.getMessage().contains(steamOffer.getSecretCode())) {
                                //Подтверждаем сделку
                                 steamRequestsService.acceptOffer(steamOffer.getIdSteamTrade(), receivedTradeOffer.getItemsToGive().size(), gameName);
                            }
                        }
                    } else {
                        System.out.println("Список активных трейд офферов продажи равен null");
                    }
                } else {
                    System.out.println("При отправке офлайн трейда, что-то пошло не так");
                }
            } else {
                System.out.println("steamOffer равен null");
            }
        } catch (Throwable th) {
        }
    }

    public void takeBuyItem(String gameName) {
        try {
            HashSet<Long> uniqueBotId = getUniqueBotId(gameName);
            if (!uniqueBotId.isEmpty()) {
                for (Long botId : uniqueBotId) {
                    System.out.println("Есть вещи, которые необходимо забрать");
                    //Отправляем запрос на создание трейда маркету
                    SteamOffer steamOffer = marketRequestsService.sendSteamOffer("out", botId, gameName);
                    if (steamOffer != null) {
                        if (steamOffer.isRequestStatus()) {
                            //Получаем список трейдов в steam
                            List<ReceivedTradeOffer> receivedTradeOfferList = steamRequestsService.getReceivedTradeOffers();
                            if (receivedTradeOfferList != null) {
                                for (ReceivedTradeOffer receivedTradeOffer : receivedTradeOfferList) {
                                    //Проверяем соответствие трейдов
                                    if (receivedTradeOffer.getTradeOfferId() == steamOffer.getIdSteamTrade()
                                            && receivedTradeOffer.getMessage().contains(steamOffer.getSecretCode())) {
                                        //Подтверждаем сделку
                                        steamRequestsService.acceptOffer(steamOffer.getIdSteamTrade(), receivedTradeOffer.getItemsToReceiveList().size(), gameName);
                                    }
                                }
                            } else {
                                System.out.println("Список активных трейд офферов покупки равен null");
                            }
                        } else {
                            System.out.println("При отправке офлайн трейда, что-то пошло не так");
                        }
                    } else {
                        System.out.println("steamOffer равен null");
                    }
                }
            } else {
                System.out.println("Нет предметов для вывода");
            }

        } catch (Throwable th) {
        }
    }

    //Получение списка уникальных botId
    public HashSet<Long> getUniqueBotId(String gameName) {
        List<TradeInfo> tradeInfoList = marketRequestsService.getTrades(gameName);
        HashSet<Long> hashSet = new HashSet<>();
        if (tradeInfoList != null && tradeInfoList.size() > 0) {
            for (TradeInfo tradeInfo : tradeInfoList) {
                //Ищем предметы со статусом 4(вещи, которые можно выводить)
                if (tradeInfo.getStatus() == 4 && tradeInfo.getBotId() != 0 && tradeInfo.getBotId() != 0
                        && tradeInfo.getBotId() != 0 && tradeInfo.getBotId() != 0
                        && tradeInfo.getBotId() != 0) {
                    hashSet.add(tradeInfo.getBotId());
                }
            }
        } else {
            System.out.println("Список трейда равен null");
        }
        return hashSet;
    }

    public void checkAdConfirmOpskinsTrade() {
        List<ReceivedTradeOffer> receivedTradeOfferList = steamRequestsService.getReceivedTradeOffers();
        if (receivedTradeOfferList != null) {
            for (ReceivedTradeOffer receivedTradeOffer : receivedTradeOfferList) {
                if (receivedTradeOffer.getTradeOfferState() == 2) {
                    System.out.println(receivedTradeOffer.getMessage());
                    String[] isbnParts = receivedTradeOffer.getMessage().split(" ");
                    System.out.println(isbnParts.length);
                    if (isbnParts.length > 7) {
                        System.out.println(isbnParts[9]);
                        if (isbnParts[9].equals("OPSkins.")) {
                            System.out.println("Вхождение OPSkins. найдено принимаем сделку");
                            steamRequestsService.acceptOffer(receivedTradeOffer.getTradeOfferId(), 0, "OPSkins");
                        }
                    }
                    if (isbnParts.length < 7){
                        if(isbnParts[2].equals("LOOT.Farm.")){
                            System.out.println("Вхождение LOOT.Farm. найдено принимаем сделку");
                            steamRequestsService.acceptOffer(receivedTradeOffer.getTradeOfferId(), 0, "LOOTFarm");
                        }
                        if(isbnParts[3].equals("CS.MONEY")){
                            System.out.println("Вхождение CS.MONEY найдено принимаем сделку");
                            steamRequestsService.acceptOffer(receivedTradeOffer.getTradeOfferId(), 0, "CS.MONEY");
                        }
                    }
                }
            }
        }
    }

    //Проверка очереди
    public void checkTrades(String gameName) {
        List<TradeInfo> tradeInfoList = marketRequestsService.getTrades(gameName);
        int sellCount = 0;
        int buyCount = 0;
        if (tradeInfoList != null && tradeInfoList.size() > 0) {
            for (TradeInfo tradeInfo : tradeInfoList) {
                if (tradeInfo.getStatus() == 2) {
                    sellCount++;
                } else if (tradeInfo.getStatus() == 4) {
                    buyCount++;
                }
            }
        }
        if (gameName.equals("CsGo")) {
            if (taskService.sellItemQueueCsGo.get() != sellCount) {
                System.out.println("Имеются несовпадения в очереди CsGo на продажу, вносим изменения");
                taskService.sellItemQueueCsGo.set(sellCount);
            } else {
                System.out.println("Все в порядке, очередь CsGo продаж совпадает");
            }
            if (taskService.buyItemQueueCsGo.get() != buyCount) {
                System.out.println("Имеются несовпадения в очереди CsGo на покупку, вносим изменения");
                taskService.buyItemQueueCsGo.set(buyCount);
            } else {
                System.out.println("Все в порядке, очередь CsGo покупок совпадает");
            }
        } else if (gameName.equals("Dota")) {
            if (taskService.sellItemQueueDota.get() != sellCount) {
                System.out.println("Имеются несовпадения в очереди Dota на продажу, вносим изменения");
                taskService.sellItemQueueDota.set(sellCount);
            } else {
                System.out.println("Все в порядке, очередь Dota продаж совпадает");
            }
            if (taskService.buyItemQueueDota.get() != buyCount) {
                System.out.println("Имеются несовпадения в очереди Dota на покупку, вносим изменения");
                taskService.buyItemQueueDota.set(buyCount);
            } else {
                System.out.println("Все в порядке, очередь Dota покупок совпадает");
            }
        } else if (gameName.equals("Pubg")) {
            if (taskService.sellItemQueuePubg.get() != sellCount) {
                System.out.println("Имеются несовпадения в очереди Pubg на продажу, вносим изменения");
                taskService.sellItemQueuePubg.set(sellCount);
            } else {
                System.out.println("Все в порядке, очередь Pubg продаж совпадает");
            }
            if (taskService.buyItemQueuePubg.get() != buyCount) {
                System.out.println("Имеются несовпадения в очереди Pubg на покупку, вносим изменения");
                taskService.buyItemQueuePubg.set(buyCount);
            } else {
                System.out.println("Все в порядке, очередь Pubg покупок совпадает");
            }
        }
    }

}
