package my.home.project.main.bot.parsing.handler;

import my.home.project.main.bot.parsing.model.ItemStatusGo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import my.home.project.main.bot.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class ItemStatusGoHandlerPubg extends NotificationHandler<ItemStatusGo.Data> {

    @Autowired
    TaskService taskService;
    
    @Inject
    public ItemStatusGoHandlerPubg(@Value("my.home.project.main.bot.parsing.model.ItemStatusGo.Data") Class<ItemStatusGo.Data> typeParameterClass) {
        super(typeParameterClass);
    }


    @Override
    protected void handle(ItemStatusGo.Data notification) {
        if(notification.getStatus() == 4 && notification.getLeft() == 14400){
            taskService.buyItemQueuePubg.getAndIncrement();
        }
    }
}
