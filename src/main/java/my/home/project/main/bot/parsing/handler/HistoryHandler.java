package my.home.project.main.bot.parsing.handler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.inject.Inject;

@Component
public class HistoryHandler extends NotificationHandler<String> {

    @Inject
    public HistoryHandler(@Value("java.lang.String") Class<String> typeParameterClass) {
        super(typeParameterClass);
    }


    @Override
    protected void handle(String notification) {
//        System.out.println(notification);
    }

}
