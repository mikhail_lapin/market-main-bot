package my.home.project.main.bot.parsing.model;

import lombok.Data;

@Data
public class RootNotification {

    private Notification.Type type;
    private String data;
}
