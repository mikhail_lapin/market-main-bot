package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemInfo {

    @JsonProperty("classid")
    private long classid;

    @JsonProperty("instanceid")
    private long instanceid;

    @JsonProperty("our_market_instanceid")
    private Long ourMarketInstanceid;

    @JsonProperty("market_name")
    private String marketName;

    @JsonProperty("name")
    private String name;

    @JsonProperty("market_hash_name")
    private String marketHashName;

    @JsonProperty("rarity")
    private String rarity;

    @JsonProperty("quality")
    private String quality;

    @JsonProperty("type")
    private String type;

    @JsonProperty("slot")
    private String slot;

    @JsonProperty("hash")
    protected String hash;

    @JsonProperty("min_price")
    private int minPrice;

    @JsonProperty("offers")
    private List<ItemOffer> offers;

    @JsonProperty("buy_offers")
    private List<BuyOffer> buyOffers;

    @JsonProperty("error")
    private String error;

}
