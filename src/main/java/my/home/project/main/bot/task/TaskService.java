package my.home.project.main.bot.task;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import my.home.project.main.bot.item.ItemToBuyService;
import my.home.project.main.bot.market.MarketRequestService;
import my.home.project.main.bot.market.TradeOfferService;
import my.home.project.main.bot.market.model.BuyItemInfo;
import my.home.project.main.bot.market.model.ItemToBuy;
import my.home.project.main.bot.market.model.NotificationInfo;
import my.home.project.main.bot.market.model.NotificationList;
import my.home.project.main.bot.market.model.Order;
import my.home.project.main.bot.market.model.PingPong;
import my.home.project.main.bot.market.model.ProcessOrder;
import my.home.project.main.bot.market.model.RefreshInventory;
import my.home.project.main.bot.parsing.model.Notification;
import my.home.project.main.bot.steam.login.CookieService;
import my.home.project.main.bot.websocket.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaskService {

    public static AtomicInteger sellItemQueueCsGo = new AtomicInteger(0);
    public static AtomicInteger buyItemQueueCsGo = new AtomicInteger(0);

    public static AtomicInteger sellItemQueueDota = new AtomicInteger(0);
    public static AtomicInteger buyItemQueueDota = new AtomicInteger(0);

    public static AtomicInteger sellItemQueuePubg = new AtomicInteger(0);
    public static AtomicInteger buyItemQueuePubg = new AtomicInteger(0);

    @Autowired
    MarketRequestService marketRequestsService;

    @Autowired
    WebSocketService webSocketService;

    @Autowired
    ItemToBuyService itemToBuyService;

    @Autowired
    TradeOfferService tradeOfferService;

    @Autowired
    CookieService cookieService;

    @Scheduled(initialDelay = 500, fixedDelay = 60000)
    public void checkOrders() {
        List<NotificationInfo> notificationInfos = marketRequestsService.getNotificationsInfo("Pubg").getNotificationsInfoList();
        for (NotificationInfo notificationInfo : notificationInfos) {
            ProcessOrder processOrder = marketRequestsService.processOrder(notificationInfo.getClassid(), notificationInfo.getInstanceid(), notificationInfo.getPrice(), "Pubg");
            System.out.println(processOrder);
        }
    }

    @Scheduled(initialDelay = 500, fixedDelay = Long.MAX_VALUE)
    public void connect() {
        try {
            tradeOfferService.checkTrades("CsGo");
            tradeOfferService.checkTrades("Dota");
            tradeOfferService.checkTrades("Pubg");
            webSocketService.getConnect();
            cookieService.writeCookies();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 5000, fixedRate = 180000)
    public void sendPingPong() {
        try {
            PingPong pingPongCsGO = marketRequestsService.sendPingPong("CsGo");
            RefreshInventory refreshInventory = marketRequestsService.refreshInventory("CsGo");
            if (refreshInventory != null) {
                if (refreshInventory.getSuccess()) {
                    System.out.println("Запрос на обновление инвентаря CsGo, успешно отправлен");
                }
            }
            if (pingPongCsGO != null) {
                System.out.println("Отправляем пинг CsGo, получаем " + pingPongCsGO.getPing());
            }
            PingPong pingPongDota = marketRequestsService.sendPingPong("Dota");
            refreshInventory = marketRequestsService.refreshInventory("Dota");
            if (refreshInventory != null) {
                if (refreshInventory.getSuccess()) {
                    System.out.println("Запрос на обновление инвентаря Dota, успешно отправлен");
                }
            }
            if (pingPongDota != null) {
                System.out.println("Отправляем пинг Dota, получаем " + pingPongDota.getPing());
            }
            PingPong pingPongPubg = marketRequestsService.sendPingPong("Pubg");
            refreshInventory = marketRequestsService.refreshInventory("Pubg");
            if (refreshInventory != null) {
                if (refreshInventory.getSuccess()) {
                    System.out.println("Запрос на обновление инвентаря Pubg, успешно отправлен");
                }
            }
            if (pingPongPubg != null) {
                System.out.println("Отправляем пинг Pubg, получаем " + pingPongPubg.getPing());
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 20000, fixedDelay = 45000)
    public void passSellItem() {
        try {
            if (sellItemQueueCsGo.get() > 0) {
                System.out.println("Есть проданные предметы CsGo для передачи " + sellItemQueueCsGo.get());
                tradeOfferService.passSellItem("CsGo");
            }
            if (sellItemQueueDota.get() > 0) {
                System.out.println("Есть проданные предметы Dota для передачи " + sellItemQueueDota.get());
                tradeOfferService.passSellItem("Dota");
            }
            if (sellItemQueuePubg.get() > 0) {
                System.out.println("Есть проданные предметы Pubg для передачи " + sellItemQueuePubg.get());
                tradeOfferService.passSellItem("Pubg");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 15000, fixedDelay = 45000)
    public void takeBuyItem() {
        try {
            if (buyItemQueueCsGo.get() > 0) {
                System.out.println("Есть купленные предметы CsGo для передачи " + buyItemQueueCsGo.get());
                tradeOfferService.takeBuyItem("CsGo");
            }
            if (buyItemQueueDota.get() > 0) {
                System.out.println("Есть купленные предметы Dota для передачи " + buyItemQueueDota.get());
                tradeOfferService.takeBuyItem("Dota");
            }
            if (buyItemQueuePubg.get() > 0) {
                System.out.println("Есть купленные предметы Pubg для передачи " + buyItemQueuePubg.get());
                tradeOfferService.takeBuyItem("Pubg");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 300000, fixedDelay = 300000)
    public void checkTrades() {
        try {
            tradeOfferService.checkTrades("CsGo");
            tradeOfferService.checkTrades("Dota");
            tradeOfferService.checkTrades("Pubg");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 10000, fixedDelay = 45000)
    public void checkAdConfirmOpskinsTrade() {
        try {
            tradeOfferService.checkAdConfirmOpskinsTrade();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

}
