package my.home.project.main.bot.websocket;

import my.home.project.main.bot.parsing.handler.HandlerRepository;
import my.home.project.main.bot.parsing.model.RootNotification;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import java.io.IOException;
import my.home.project.main.bot.util.Util;
import my.home.project.main.bot.market.MarketRequestService;
import my.home.project.main.bot.market.model.WSAuth;
import my.home.project.main.bot.parsing.util.MapperUtils;

@WebSocket
public class Handler {

    private Session session;

    @OnWebSocketConnect
    public void onConnect(Session session) {

        session.setIdleTimeout(-1);
        System.out.println("Connected to websocket");
        this.session = session;

        try {

            MarketRequestService marketRequestService = Util.staticAppContext.getBean(MarketRequestService.class);

            WSAuth auth = marketRequestService.getWSAuth();
            System.out.println(auth.getWsAuth());
            sendClient(auth.getWsAuth());

            String newItemsCsGo = "newitems_go";
            String newItemsDota = "newitems_cs";
            String newItemsPubg = "newitems_pb";
            System.out.println(newItemsCsGo);
            sendClient(newItemsCsGo);
            System.out.println(newItemsDota);
            sendClient(newItemsDota);
            System.out.println(newItemsPubg);
            sendClient(newItemsPubg);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @OnWebSocketMessage
    public void onMessage(String message) throws IOException {
//        System.out.println(message);
        try {
            RootNotification rNotif = MapperUtils.getMapper().readValue(message, RootNotification.class);
            HandlerRepository handlerRepository = Util.staticAppContext.getBean(HandlerRepository.class);
            if (handlerRepository.get(rNotif.getType()) != null) {
                handlerRepository.get(rNotif.getType()).processNotification(rNotif.getData());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    @OnWebSocketError
    public void onError(Throwable error) {
        System.out.println("Error: " + error);
        session.close();
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        try {

            System.out.println("WebSocket Closed: " + statusCode + " - " + reason);

            Thread.sleep(5000);

            new WebSocketService().getConnect();

        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    public void sendClient(String str) {

        try {
            if (session.isOpen()) {
                this.session.getRemote().sendString(str);
            } else {
                System.out.println("WebSocket не подключены");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }
}
