package my.home.project.main.bot.parsing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@ToString(callSuper = true)
public class AddItem extends Notification<AddItem.Data> {

    @lombok.Data
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data implements NotificationData {

        @JsonProperty("ui_id")
        private int uiId;

        @JsonProperty("i_name")
        private String name;

        @JsonProperty("ui_status")
        private int uiStatus;

        @JsonProperty("ui_price")
        private double uiPrice;

        @JsonProperty("i_classid")
        private Long classId;

        @JsonProperty("i_instanceid")
        private Long instanceId;

        @JsonProperty("ui_uid")
        private int uiUId;

        @JsonProperty("ui_bid")
        private int uiBid;

    }

}
