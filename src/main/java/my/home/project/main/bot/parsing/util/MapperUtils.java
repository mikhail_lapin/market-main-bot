package my.home.project.main.bot.parsing.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class MapperUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {

        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

    }

    public static ObjectMapper getMapper() {

        return mapper;

    }

    public static <T> T parse(String value, Class<T> clazz) {

        try {

            return mapper.readValue(value, clazz);

        } catch (IOException ex) {

            ex.printStackTrace();

            return null;

        }

    }

}
