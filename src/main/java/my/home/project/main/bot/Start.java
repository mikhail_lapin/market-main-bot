package my.home.project.main.bot;

import java.io.File;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Start {

    public static void main(String[] args) {
        System.setProperty("bot.cfg", new File(Start.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent() + "//config.properties");
        ApplicationContext ac = new ClassPathXmlApplicationContext("application-context.xml");
    }

}
