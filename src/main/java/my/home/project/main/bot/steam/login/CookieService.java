package my.home.project.main.bot.steam.login;

import org.apache.http.client.CookieStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
public class CookieService {

    @Autowired
    SteamLogin steamLogin;

    CookieStore cookieStore;

    public CookieStore getCookieStore() {
        return cookieStore;
    }

    public void setCookieStore(CookieStore cookieStore) {
        this.cookieStore = cookieStore;
    }

    public CookieStore writeCookies() {
        try {
            cookieStore = steamLogin.doLogin();
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException e) {
            e.printStackTrace();
        }
        if (cookieStore != null) {
            return cookieStore;
        }
        return null;
    }

}
