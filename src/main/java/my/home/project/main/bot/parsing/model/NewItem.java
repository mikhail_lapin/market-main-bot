package my.home.project.main.bot.parsing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@ToString(callSuper = true)
public class NewItem extends Notification<NewItem.Data> {

    @lombok.Data
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data implements NotificationData {

        @JsonProperty("app")
        private String app;

        @JsonProperty("i_classid")
        private Long classId;

        @JsonProperty("i_instanceid")
        private Long instanceId;

        @JsonProperty("i_market_hash_name")
        private String iMarketHashName;

        @JsonProperty("i_market_name")
        private String iMarketName;

        @JsonProperty("ui_price")
        private Double uiPrice;

        @JsonProperty("i_quality")
        private String quality;
    }

}
