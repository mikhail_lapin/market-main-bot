package my.home.project.main.bot.requester;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

@Component
public class SendingRequestService {

    private final CloseableHttpClient httpclient = HttpClients.createDefault();

    public InputStream sendRequest(String request) {
        InputStream is = null;
        HttpGet httpGet = new HttpGet(request);
        CloseableHttpResponse response;
        int stopCount = 0;
        try {
            response = httpclient.execute(httpGet);
            while (response == null && stopCount != 3) {
                response.close();
                response = httpclient.execute(httpGet);
                stopCount++;
            }
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                is = response.getEntity().getContent();
            } else {
                response.close();
                System.out.println("Ошибка - запрос не удался!");
                return null;
            }
        } catch (Throwable th) {
            System.out.println("Ошибка в: sendRequest " + th);
        }
        return is;
    }

    public InputStream sendTestRequest(String marketUrl, String request) {
        HttpGet httpGet = new HttpGet(marketUrl + request);
        InputStream is = null;
        CloseableHttpResponse response;
        try {
            response = httpclient.execute(httpGet);
            is = response.getEntity().getContent();
        } catch (IOException ex) {
        }
        httpGet.abort();
        return is;
    }

}
