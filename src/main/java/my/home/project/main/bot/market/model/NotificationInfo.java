package my.home.project.main.bot.market.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationInfo {

    @JsonProperty("i_classid")
    private long classid;

    @JsonProperty("i_instanceid")
    private long instanceid;

    @JsonProperty("i_market_hash_name")
    private String iMarketHashName;

    @JsonProperty("i_market_name")
    private String marketName;

    @JsonProperty("n_val")
    private int price;
}
