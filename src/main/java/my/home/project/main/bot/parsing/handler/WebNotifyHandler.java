package my.home.project.main.bot.parsing.handler;

import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import my.home.project.main.bot.item.ItemToBuyService;
import my.home.project.main.bot.market.MarketRequestService;
import my.home.project.main.bot.market.model.BuyItemInfo;
import my.home.project.main.bot.market.model.ItemToBuy;
import my.home.project.main.bot.parsing.model.WebNotify;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class WebNotifyHandler extends NotificationHandler<WebNotify.Data> {

    @Inject
    public WebNotifyHandler(@Value("my.home.project.main.bot.parsing.model.WebNotify.Data") Class<WebNotify.Data> typeParameterClass) {
        super(typeParameterClass);
    }

    @Autowired
    ItemToBuyService itemToBuyService;

    @Autowired
    MarketRequestService marketRequestsService;

    @Override
    protected void handle(WebNotify.Data notification) {
        Map<String, ItemToBuy> itemToBuyMapIdsCsGo = itemToBuyService.getItemToBuyMapIdsCsGo();
        try {
            if (itemToBuyMapIdsCsGo.get(notification.getTag()) != null) {
                ItemToBuy itemToBuy = itemToBuyMapIdsCsGo.get(notification.getTag());
                BuyItemInfo buyItemInfo = marketRequestsService.buyItem(itemToBuy, "CsGo");
                System.out.println("Попытка покупки " + itemToBuy.getMarketName() + " через WebNotify" + buyItemInfo.getResult());
            }
        } catch (Throwable th) {
            System.out.println("Проеба в: WebNotifyHandler");
        }

        Map<String, ItemToBuy> itemToBuyMapIdsPubg = itemToBuyService.getItemToBuyMapIdsPubg();
        try {
            if (itemToBuyMapIdsPubg.get(notification.getTag()) != null) {
                ItemToBuy itemToBuy = itemToBuyMapIdsPubg.get(notification.getTag());
                BuyItemInfo buyItemInfo = marketRequestsService.buyItem(itemToBuy, "Pubg");
                System.out.println("Попытка покупки " + itemToBuy.getMarketName() + " через WebNotify" + buyItemInfo.getResult());
            }
        } catch (Throwable th) {
            System.out.println("Проеба в: WebNotifyHandler");
        }

        Map<String, ItemToBuy> itemToBuyMapIdsDota = itemToBuyService.getItemToBuyMapIdsDota();
        try {
            if (itemToBuyMapIdsDota.get(notification.getTag()) != null) {
                ItemToBuy itemToBuy = itemToBuyMapIdsDota.get(notification.getTag());
                BuyItemInfo buyItemInfo = marketRequestsService.buyItem(itemToBuy, "Dota");
                System.out.println("Попытка покупки " + itemToBuy.getMarketName() + " через WebNotify" + buyItemInfo.getResult());
            }
        } catch (Throwable th) {
            System.out.println("Проеба в: WebNotifyHandler");
        }
    }

}
