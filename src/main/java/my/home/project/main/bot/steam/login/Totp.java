package my.home.project.main.bot.steam.login;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
public class Totp {

    @Value("${sharedSecret}")
    private String sharedSecret;
    @Value("${identitySecret}")
    private String identitySecret;

    public String getAuthCode(long time) {
        int timeInt = (int) (time / 1000);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (DataOutputStream dos = new DataOutputStream(baos)) {
            dos.writeInt(0);
            dos.writeInt((int) Math.floor(timeInt / 30));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Mac mac = null;
        try {
            mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec signingKey = new SecretKeySpec(Base64.decodeBase64(sharedSecret), "HmacSHA1");
            mac.init(signingKey);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        byte[] result = mac.doFinal(baos.toByteArray());
        int start = result[19] & 0x0f;
        byte[] croppedResult = new byte[4];
        System.arraycopy(result, start, croppedResult, 0, croppedResult.length);
        int readed = 0;
        try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(croppedResult))) {
            readed = dis.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long fullcode = (readed & 0x00000000ffffffffL) & 0x7fffffff;
        String chars = "23456789BCDFGHJKMNPQRTVWXY";
        String code = "";
        for (int i = 0; i < 5; i++) {
            code += chars.charAt((int) fullcode % chars.length());
            fullcode /= chars.length();
        }
        return code;
    }

    public String getConfirmationKey(long time, String tag) {
        int timeInt = (int) (time / 1000);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (DataOutputStream dos = new DataOutputStream(baos)) {
            dos.writeInt(0);
            dos.writeInt(timeInt);
            dos.write(tag.getBytes(Charset.forName("ASCII")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Mac mac = null;
        try {
            mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec signingKey = new SecretKeySpec(Base64.decodeBase64(identitySecret), "HmacSHA1");
            mac.init(signingKey);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return Base64.encodeBase64String(mac.doFinal(baos.toByteArray()));
    }

    public static String bytes2String(byte[] bb) {
        return Hex.encodeHexString(bb);
    }

}
