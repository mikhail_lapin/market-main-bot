package my.home.project.main.bot.steam;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import my.home.project.main.bot.parsing.JsonParsingService;
import my.home.project.main.bot.requester.SendingRequestService;
import my.home.project.main.bot.steam.login.Confirmation;
import my.home.project.main.bot.steam.login.CookieService;
import my.home.project.main.bot.steam.login.model.ConfirmResult;
import my.home.project.main.bot.steam.login.model.ConfirmationInfo;
import my.home.project.main.bot.steam.model.AcceptResalt;
import my.home.project.main.bot.steam.model.ReceivedTradeOffer;
import my.home.project.main.bot.steam.model.SteamInventory;
import my.home.project.main.bot.steam.model.SteamInventoryItem;
import my.home.project.main.bot.steam.model.SteamRgInventory;
import my.home.project.main.bot.steam.model.SteamTradeResponse;
import my.home.project.main.bot.task.TaskService;

@Component
public class SteamRequestsService {

    JsonParsingService jsonParsingService = new JsonParsingService();

    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    @Autowired
    SendingRequestService sendingRequestService;

    @Autowired
    CookieService cookieService;

    @Autowired
    Confirmation confirmation;

    @Autowired
    TaskService taskService;

    @Value("${steamKey}")
    private String steamKey;

    @Value("${steamId}")
    private String steamId;

    @Value("${sessionId}")
    private String sessionId;

    public List<ReceivedTradeOffer> getReceivedTradeOffers() {
        List<ReceivedTradeOffer> receivedTradeOfferList = new ArrayList<>();
        String request = "http://api.steampowered.com/IEconService/GetTradeOffers/v1/?key=" + steamKey + "&format=json&get_received_offers=1&active_only=1";
        InputStream is = sendingRequestService.sendRequest(request);
        SteamTradeResponse steamTradeResponse = null;
        try {
            steamTradeResponse = jsonParsingService.parse(is, SteamTradeResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (steamTradeResponse != null) {
            return steamTradeResponse.getReceivedTradeOffers().getReceivedTradeOfferList();
        }
        return null;
    }

    public Boolean acceptOffer(long tradeofferid, int size, String gameName) {
        Boolean checkTradeStatus = false;
        String url = "https://steamcommunity.com/tradeoffer/" + tradeofferid + "/accept";
        String sessionid = "";
        CookieStore cookieStore = cookieService.getCookieStore();
        if (cookieStore != null) {
            for (Cookie cookie : cookieStore.getCookies()) {
                if (cookie.getName().equals("sessionid")) {
                    sessionid = cookie.getValue();
                }
            }
        } else {
            System.out.println("Не удалось получить куки...");
        }
        //Формирование полей для POST запроса
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("sessionid", sessionid));
        nvps.add(new BasicNameValuePair("serverid", "1"));
        nvps.add(new BasicNameValuePair("tradeofferid", String.valueOf(tradeofferid)));
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        HttpContext localContext = new BasicHttpContext();
        //Вставляем Cookie в HttpContext
        localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
        //Добавление хедеров в POST запрос
        httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
        httpPost.setHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        httpPost.setHeader("Referer", "https://steamcommunity.com/tradeoffer/" + tradeofferid + "/");
//      Добавление полей в POST запрос
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
        AcceptResalt acceptResalt = new AcceptResalt();
        try {
//          Отправка POST запроса вместе с HttpContext
            CloseableHttpResponse response = httpclient.execute(httpPost, localContext);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                acceptResalt = objectMapper.readValue(entity.getContent(), new TypeReference<AcceptResalt>() {
                });
                if (acceptResalt != null) {
                    if (acceptResalt.getStrError() != null) {
                        System.out.println("Ошибка " + response.getStatusLine().getStatusCode() + " " + acceptResalt.getStrError());
                    } else if (acceptResalt.getTradeid() != null) {
                        System.out.println("Сделка принята, Trade Id = " + acceptResalt.getTradeid());
                        checkTradeStatus = true;
                        if (gameName.equals("CsGo")) {
                            for (int i = 0; i < size; i++) {
                                taskService.buyItemQueueCsGo.getAndDecrement();
                            }
                        } else if (gameName.equals("Dota")) {
                            for (int i = 0; i < size; i++) {
                                taskService.buyItemQueueDota.getAndDecrement();
                            }
                        }
                    } else if (acceptResalt.isNeedsMobileConfirmation()) {
                        Thread.sleep(10000);
                        int count = 0;
                        long time;
                        List<ConfirmationInfo> confirmationInfoList = null;
                        do {
                            time = System.currentTimeMillis();
                            confirmationInfoList = confirmation.getConfirmations(time);
                            System.out.println("Сделок для подтверждения = " + confirmationInfoList.size());
                            count++;
                        } while (count < 3 && (confirmationInfoList == null || confirmationInfoList.size() < 1));
                        if (confirmationInfoList != null && confirmationInfoList.size() != 0) {
                            for (ConfirmationInfo confirmationInfo : confirmationInfoList) {
                                ConfirmResult confirmResult = null;
                                count = 0;
                                do {
                                    time = System.currentTimeMillis();
                                    confirmResult = confirmation.acceptConfirm(time, confirmationInfo.getConfId(), confirmationInfo.getKey());
                                    count++;
                                } while (count < 3 && (confirmResult == null || !confirmResult.isSuccess()));
                                if (confirmResult != null && confirmResult.isSuccess()) {
                                    System.out.println("Сделка успешко подтверждена, Trade Id = " + tradeofferid);
                                    checkTradeStatus = true;
                                    if (gameName.equals("CsGo")) {
                                        for (int i = 0; i < size; i++) {
                                            taskService.sellItemQueueCsGo.getAndDecrement();
                                        }
                                    } else if (gameName.equals("Dota")) {
                                        for (int i = 0; i < size; i++) {
                                            taskService.sellItemQueueDota.getAndDecrement();
                                        }
                                    }
                                } else {
                                    System.out.println("Ошибка: confirmResult = " + confirmResult);
                                }
                            }
                        } else {
                            System.out.println("confirmationInfoList = null");
                        }
                    } else {
                        System.out.println("Ошибка " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
                        System.out.println("Получаем новые куки...");
                        cookieService.writeCookies();
                    }
                } else {
                    System.out.println("acceptResalt = null");
                    System.out.println("Получаем новые куки...");
                    cookieService.writeCookies();
                }
            }
        } catch (Throwable e) {
        }
        return checkTradeStatus;
    }

    public List<SteamInventoryItem> getInventoryItems() {
        List<SteamInventoryItem> inventoryItemList = new ArrayList<>();
        String request = "http://steamcommunity.com/profiles/" + steamId + "/inventory/json/730/2/?l=russian";
        InputStream is = sendingRequestService.sendRequest(request);
        SteamInventory inventory = new SteamInventory();
        try {
            inventory = jsonParsingService.parse(is, SteamInventory.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Map.Entry<String, SteamRgInventory> entry : inventory.getRgInventory().entrySet()) {
            SteamRgInventory steamRgInventory = entry.getValue();
            for (Map.Entry<String, SteamInventoryItem> entryItems : inventory.getRgDescriptions().entrySet()) {
                SteamInventoryItem steamInventoryItem = entryItems.getValue();
                if (steamRgInventory.getClassId() == steamInventoryItem.getClassId()
                        && steamRgInventory.getInstanceId() == steamInventoryItem.getInstanceId()) {
                    inventoryItemList.add(entryItems.getValue());
                }
            }
        }
        return inventoryItemList;
    }

}
