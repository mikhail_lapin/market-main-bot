package my.home.project.main.bot.market.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@AllArgsConstructor
@NoArgsConstructor

@JsonIgnoreProperties(ignoreUnknown = true)
public class WSAuth {

    @JsonProperty("wsAuth")
    private String wsAuth;

    @JsonProperty("success")
    private Boolean success;

    @JsonProperty("error")
    private String error;

}
