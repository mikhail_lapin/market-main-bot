package my.home.project.main.bot.parsing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.ToString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(name = "additem_go", value = AddItem.class),
    @JsonSubTypes.Type(name = "additem_cs", value = AddItem.class),
    @JsonSubTypes.Type(name = "additem_pb", value = AddItem.class),
    @JsonSubTypes.Type(name = "history", value = History.class),
    @JsonSubTypes.Type(name = "invcache_go", value = InvCache.class),
    @JsonSubTypes.Type(name = "invcache_cs", value = InvCache.class),
    @JsonSubTypes.Type(name = "invcache_pb", value = InvCache.class),
    @JsonSubTypes.Type(name = "itemout_new_go", value = ItemOutNew.class),
    @JsonSubTypes.Type(name = "itemout_new_cs", value = ItemOutNew.class), 
    @JsonSubTypes.Type(name = "itemout_new_pb", value = ItemOutNew.class), 
    @JsonSubTypes.Type(name = "itemstatus_go", value = ItemStatusGo.class),
    @JsonSubTypes.Type(name = "itemstatus_cs", value = ItemStatusGo.class),
    @JsonSubTypes.Type(name = "itemstatus_pb", value = ItemStatusGo.class),
    @JsonSubTypes.Type(name = "money", value = Money.class),
    @JsonSubTypes.Type(name = "newitems_go", value = NewItem.class),
    @JsonSubTypes.Type(name = "newitems_cs", value = NewItem.class),
    @JsonSubTypes.Type(name = "newitems_pb", value = NewItem.class),
    @JsonSubTypes.Type(name = "webnotify", value = WebNotify.class)
})
@Data
@ToString
public class Notification<T> {

    private Type type;
    private T data;

    public enum Type {

        ADD_ITEM_CSGO("additem_go"),
        ADD_ITEM_DOTA("additem_cs"),
        ADD_ITEM_PUBG("additem_pb"),
        HISTORY("history"),
        INVCACHE_CSGO("invcache_go"),
        INVCACHE_DOTA("invcache_cs"),
        INVCACHE_PUBG("invcache_pb"),
        ITEM_OUT_NEW_CSGO("itemout_new_go"),
        ITEM_OUT_NEW_DOTA("itemout_new_cs"),
        ITEM_OUT_NEW_PUBG("itemout_new_pb"),
        ITEM_STATUS_CSGO("itemstatus_go"),
        ITEM_STATUS_DOTA("itemstatus_cs"),
        ITEM_STATUS_PUBG("itemstatus_pb"),
        MONEY("money"),
        NEW_ITEM_CSGO("newitems_go"),
        NEW_ITEM_DOTA("newitems_cs"),
        NEW_ITEM_PUBG("newitems_pb"),
        WEBNOTIFY("webnotify");
        

        public final String value;

        private Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @JsonCreator
        public static Type byValue(String value) {
            for (Type type : values()) {
                if (type.getValue().equals(value)) {
                    return type;
                }
            }
            return null;
        }

    }
}

