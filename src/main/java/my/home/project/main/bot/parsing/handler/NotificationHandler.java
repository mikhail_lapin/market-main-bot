package my.home.project.main.bot.parsing.handler;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public abstract class NotificationHandler<T> {

    private final Class<T> typeParameterClass;
    private static final ObjectMapper om = new ObjectMapper();

    public NotificationHandler(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    private T parse(String data) throws IOException {
        return om.readValue(data, typeParameterClass);
    }

    public void processNotification(String data) {
        T notif = null;
        /*data = data
                .replace("\"{", "{")
                .replace("}\"", "}")
                .replace("\\\\\\\"\\\\", "\\\"\\")
                .replace("\\\\\"", "\"")
                .replace("\\u", "\\\\u")
                .replace("\\\\\\\\u", "\\\\u")
                .replace("\\\\\\u", "\\\\u")
                .replace("\\\\u", "\\u")
                .replace("\\\\\":", "\":")
                .replace("{\\\"", "{\"")
                .replace(":\\\"", ":\"")
                .replace("\\\\\\\\\\\\\"\\", "\\\"\\")
                .replace(",\\\\\"", ",\"")
                .replace("\\\",", "\",")
                .replace("\\\\\\\\\\\\/", "/")
                .replace("/\\\\", "")
                .replace("\\\\\\\\/", "//")
                .replace("\\\\\\/", "/")
                .replace("\",\\\"", "\",\"")
                .replace("\\\":", "\":")
                .replace("\\\"}", "\"}")
                .replace("\\\"The End of the Line\"", "");*/
        

        try {
            notif = parse(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        handle(notif);
    }

    protected abstract void handle(T notification);
}
