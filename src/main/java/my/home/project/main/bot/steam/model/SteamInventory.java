package my.home.project.main.bot.steam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SteamInventory {

    @JsonProperty("success")
    private String success;

    @JsonProperty("rgInventory")
    private Map<String, SteamRgInventory> rgInventory;

    @JsonProperty("rgDescriptions")
    private Map<String, SteamInventoryItem> rgDescriptions;
    
}
