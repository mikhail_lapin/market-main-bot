package my.home.project.main.bot.parsing.handler;

import my.home.project.main.bot.parsing.model.InvCache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
@Component
public class InvCacheHandlerPubg extends NotificationHandler<InvCache.Data> {

    @Inject
    public InvCacheHandlerPubg(@Value("my.home.project.main.bot.parsing.model.InvCache.Data") Class<InvCache.Data> typeParameterClass) {
        super(typeParameterClass);
    }

    @Override
    protected void handle(InvCache.Data notification) {
        System.out.println("Инвентарь Pubg обновлён: " + notification.getTime());
    }
}
