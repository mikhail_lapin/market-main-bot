package my.home.project.main.bot.steam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class AcceptResalt {

    @JsonProperty("strError")
    private String strError;

    @JsonProperty("tradeid")
    public String tradeid;

    @JsonProperty("needs_mobile_confirmation")
    public boolean needsMobileConfirmation;

    @JsonProperty("needs_email_confirmation")
    public boolean needsEmailConfirmation;

    @JsonProperty("email_domain")
    public String emailDomain;

}
