package my.home.project.main.bot.parsing.handler;

import my.home.project.main.bot.parsing.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.Map;

@Component
public class HandlerRepository {

    @Autowired
    private ApplicationContext appContext;

    private final Map<Notification.Type, NotificationHandler> handlerRepository = new EnumMap<>(Notification.Type.class);

    @PostConstruct
    public void init() {
//        handlerRepository.put(Notification.Type.HISTORY, appContext.getBean(HistoryHandler.class));
        handlerRepository.put(Notification.Type.ADD_ITEM_CSGO, appContext.getBean(AddItemHandlerCsGo.class));
        handlerRepository.put(Notification.Type.ADD_ITEM_DOTA, appContext.getBean(AddItemHandlerDota.class));
        handlerRepository.put(Notification.Type.ADD_ITEM_PUBG, appContext.getBean(AddItemHandlerPubg.class));
        handlerRepository.put(Notification.Type.INVCACHE_CSGO, appContext.getBean(InvCacheHandlerCsGo.class));
        handlerRepository.put(Notification.Type.INVCACHE_DOTA, appContext.getBean(InvCacheHandlerDota.class));
        handlerRepository.put(Notification.Type.INVCACHE_PUBG, appContext.getBean(InvCacheHandlerPubg.class));
        handlerRepository.put(Notification.Type.ITEM_OUT_NEW_CSGO, appContext.getBean(ItemOutNewHandlerCsGo.class));
        handlerRepository.put(Notification.Type.ITEM_OUT_NEW_DOTA, appContext.getBean(ItemOutNewHandlerDota.class));
        handlerRepository.put(Notification.Type.ITEM_OUT_NEW_PUBG, appContext.getBean(ItemOutNewHandlerPubg.class));
        handlerRepository.put(Notification.Type.ITEM_STATUS_CSGO, appContext.getBean(ItemStatusGoHandlerCsGo.class));
        handlerRepository.put(Notification.Type.ITEM_STATUS_DOTA, appContext.getBean(ItemStatusGoHandlerDota.class));
        handlerRepository.put(Notification.Type.ITEM_STATUS_PUBG, appContext.getBean(ItemStatusGoHandlerPubg.class));
        handlerRepository.put(Notification.Type.MONEY, appContext.getBean(MoneyHandler.class));
        handlerRepository.put(Notification.Type.NEW_ITEM_CSGO, appContext.getBean(NewItemHandlerCsGo.class));
        handlerRepository.put(Notification.Type.NEW_ITEM_DOTA, appContext.getBean(NewItemHandlerDota.class));
        handlerRepository.put(Notification.Type.NEW_ITEM_PUBG, appContext.getBean(NewItemHandlerPubg.class));
        handlerRepository.put(Notification.Type.WEBNOTIFY, appContext.getBean(WebNotifyHandler.class));
    }

    public NotificationHandler get(Notification.Type type) {
        return handlerRepository.get(type);
    }

}
