package my.home.project.main.bot.steam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SteamInventoryItem {

    @JsonProperty("appid")
    private int appId;

    @JsonProperty("classid")
    private Long classId;

    @JsonProperty("instanceid")
    private Long instanceId;

    @JsonProperty("icon_url")
    private String iconUrl;

    @JsonProperty("name")
    private String name;

    @JsonProperty("market_hash_name")
    private String marketHashName;

    @JsonProperty("market_name")
    private String marketName;

    @JsonProperty("tradable")
    private int tradable;

    @JsonProperty("marketable")
    private int marketable;

    private int amount;

}
